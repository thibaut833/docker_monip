FROM php:7-apache

RUN apt update && apt install vim -y
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli

COPY index.php /var/www/html
