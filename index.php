<?php
  $link = mysqli_connect("mysqltr", "root", "root", "bdd");
  
  if (!$link) {
    echo "Erreur : Impossible de se connecter à MySQL." . PHP_EOL;
    echo "Errno de débogage : " . mysqli_connect_errno() . PHP_EOL;
    echo "Erreur de débogage : " . mysqli_connect_error() . PHP_EOL;
    exit;
  }

  $format = ($_GET['format'] == 'json') ? 'json' : 'text';

  $ip = $_SERVER['HTTP_X_REAL_IP'];
  $user_agent = $_SERVER['HTTP_USER_AGENT'];

  $sql = "INSERT INTO monip (ip, user_agent) VALUES ('".htmlentities(addslashes($ip))."','".htmlentities(addslashes($user_agent))."')";
  if (!mysqli_query($link,$sql)) {
	  die("PB INSERT BDD");
  }

  if ($format == 'text') {
    header('Content-Type: text/plain');
    print 'IP : ' .$ip . "\n";
    print 'USER_AGENT : '.$user_agent;
  }
  if ($format == 'json') {
    $retour['ip'] = $ip;
    $retour['user-agent'] = $user_agent;
    header('Content-Type: application/json');
    print json_encode($retour);
  }
?>
