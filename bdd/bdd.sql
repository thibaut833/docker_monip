CREATE DATABASE bdd;

CREATE TABLE bdd.monip (
	id INT auto_increment NOT NULL,
	`date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	ip varchar(100) NOT NULL,
	user_agent varchar(1000) NOT NULL,
	CONSTRAINT monip_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=latin1
COLLATE=latin1_swedish_ci;